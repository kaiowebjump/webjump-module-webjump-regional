<?php
/**
 * @author      Webjump Core Team <dev@webjump.com.br>
 * @copyright   2020 Webjump (http://www.webjump.com.br)
 * @license     http://www.webjump.com.br  Copyright
 * @link        http://www.webjump.com.br
 */
declare(strict_types=1);

namespace Webjump\Regional\Ui\DataProvider\Form\Modifier;


use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Framework\App\RequestInterface;

class FieldsetCommercialsRelated extends AbstractModifier
{
    /**
     * @var RequestInterface
    */
    private $request;

    /**
     * @param RequestInterface $request
    */
    public function __construct(
        RequestInterface $request
    ) {
        $this->request = $request;
    }

    /**
     * @inheritDoc
     */
    public function modifyData(array $data): array
    {
        return $data;
    }

    /**
     * @inheritDoc
     */
    public function modifyMeta(array $meta): array
    {
        $params = $this->request->getParams();
        if (!isset($params['entity_id'])) {
            $meta['general'] = [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'visible' => false
                        ]
                    ]
                ]
            ];
        }

        return $meta;
    }
}