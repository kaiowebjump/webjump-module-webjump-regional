<?php
/**
 * Copyright (C) 2020 Webjump
 *
 * This file included in Webjump/Regional is licensed under OSL 3.0
 *
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */
declare(strict_types=1);

namespace Webjump\Regional\Ui\Component\MassAction;

use Magento\Ui\Component\MassAction\Filter as BaseFilter;
use Magento\Framework\Api\Search\DocumentInterface;

class Filter
{
    /**
     * @var BaseFilter
     */
    private $filter;

    /**
     * @param BaseFilter $filter
     */
    public function __construct(
        BaseFilter $filter
    ) {
        $this->filter = $filter;
    }

    /**
     * Get ids from search filter
     *
     * @return array
     */
    public function getIds(): array
    {
        $this->filter->applySelectionOnTargetProvider();
        $component = $this->filter->getComponent();
        $this->filter->prepareComponent($component);

        $dataProvider = $component->getContext()->getDataProvider();
        $dataProvider->setLimit(0, false);
        $searchResult = $dataProvider->getSearchResult();

        return array_map(function (DocumentInterface $item) {
            return $item->getId();
        }, $searchResult->getItems());
    }
}
