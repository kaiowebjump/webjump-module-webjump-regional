<?php
/**
 * Copyright (C) 2019 Webjump
 *
 * This file included in Webjump/Regional is licensed under OSL 3.0
 *
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */
declare(strict_types=1);

namespace Webjump\Regional\Api;


interface WebjumpRegionalRepositoryInterface
{
    /**
     * @param \Webjump\Regional\Api\Data\WebjumpRegionalInterface $webjumpRegional
     * @return int
     * @throws \Magento\Framework\Validation\ValidationException
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Webjump\Regional\Api\Data\WebjumpRegionalInterface $webjumpRegional): int;

    /**
     * @param int $webjumpRegionalId
     * @return \Webjump\Regional\Api\Data\WebjumpRegionalInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById(int $webjumpRegionalId): \Webjump\Regional\Api\Data\WebjumpRegionalInterface;

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface|null $searchCriteria
     * @return \Webjump\Regional\Api\Data\WebjumpRegionalSearchResultsInterface
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria = null
    ): \Webjump\Regional\Api\Data\WebjumpRegionalSearchResultsInterface;

    /**
     * @param int $webjumpRegionalId
     * @return void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function deleteById(int $webjumpRegionalId): void;
}