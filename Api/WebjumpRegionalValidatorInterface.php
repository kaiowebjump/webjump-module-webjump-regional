<?php
/**
 * Copyright (C) 2019 Webjump
 *
 * This file included in Webjump/Regional is licensed under OSL 3.0
 *
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */
declare(strict_types=1);

namespace Webjump\Regional\Api;

use Magento\Framework\Validation\ValidationResult;
use Webjump\Regional\Api\Data\WebjumpRegionalInterface;

interface WebjumpRegionalValidatorInterface
{
    /**
     * @param \Webjump\Regional\Api\Data\WebjumpRegionalInterface
     * @return bool
    */
    public function validate(WebjumpRegionalInterface $webjumpRegional): bool;
}