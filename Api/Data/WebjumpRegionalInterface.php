<?php
/**
 * Copyright (C) 2019 Webjump
 *
 * This file included in Webjump/Regional is licensed under OSL 3.0
 *
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */
declare(strict_types=1);

namespace Webjump\Regional\Api\Data;

interface WebjumpRegionalInterface
{
    const ENTITY_ID = 'entity_id';
    const NAME = 'name';
    const IS_ACTIVE = 'is_active';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    const PARAM_SESSION_REGIONAL_ENTITY_ID = 'webjump_regional_edit_id';

    const REGIONAL_ACTIVE = 1;
    const REGIONAL_INACTIVE = 0;


    /**
     * @return int Order ID.
     */
    public function getEntityId(): int;

    /**
     * @param int $entityId
     * @return void
     */
    public function setEntityId($entityId): void;

    /**
     * @return string
    */
    public function getName(): ?string;

    /**
     * @param string $name
     * @return void
    */
    public function setName(string $name): void;

    /**
     * @return bool|null
    */
    public function getIsActive(): ?bool;

    /**
     * @param bool|null $isActive
     */
    public function setIsActive(?bool $isActive): void;

    /**
     * @return string|null
     */
    public function getCreatedAt(): ?string;

    /**
     * @param string|null $createdAt
     * @return void
     */
    public function setCreatedAt(?string $createdAt): void;

    /**
     * @return string|null
     */
    public function getUpdatedAt(): ?string;

    /**
     * @param string|null $updatedAt
     * @return void
     */
    public function setUpdatedAt(?string $updatedAt): void;
}