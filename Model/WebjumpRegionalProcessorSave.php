<?php
/**
 * Copyright (C) 2019 Webjump
 *
 * This file included in Webjump/Regional is licensed under OSL 3.0
 *
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */
declare(strict_types=1);

namespace Webjump\Regional\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\EntityManager\EventManager;
use Webjump\Regional\Api\Data\WebjumpRegionalInterface;
use Webjump\Regional\Api\Data\WebjumpRegionalInterfaceFactory;
use Webjump\Regional\Api\WebjumpRegionalRepositoryInterface;

class WebjumpRegionalProcessorSave
{
    /**
     * @var WebjumpRegionalInterfaceFactory
     */
    private $webjumpRegionalFactory;

    /**
     * @var WebjumpRegionalRepositoryInterface
     */
    private $webjumpRegionalRepository;

    /**
     * @var DataObjectHelper
     */
    private $dataObjectHelper;

    /**
     * @var EventManager
     */
    private $eventManager;

    /**
     * @param WebjumpRegionalInterfaceFactory $webjumpRegionalFactory
     * @param WebjumpRegionalRepositoryInterface $webjumpRegionalRepository
     * @param DataObjectHelper $dataObjectHelper
     * @param EventManager $eventManager
     */
    public function __construct(
        WebjumpRegionalInterfaceFactory $webjumpRegionalFactory,
        WebjumpRegionalRepositoryInterface $webjumpRegionalRepository,
        DataObjectHelper $dataObjectHelper,
        EventManager $eventManager
    ) {
        $this->webjumpRegionalFactory = $webjumpRegionalFactory;
        $this->webjumpRegionalRepository = $webjumpRegionalRepository;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->eventManager = $eventManager;
    }

    /**
     * Save regional process action
     *
     * @param int|null $webjumpRegionalId
     * @param RequestInterface $request
     * @return int|null
     */
    public function process($webjumpRegionalId, RequestInterface $request): ?int
    {
        if (null === $webjumpRegionalId) {
            $webjumpRegional = $this->webjumpRegionalFactory->create();
        } else {
            $webjumpRegional = $this->webjumpRegionalRepository->getById($webjumpRegionalId);
        }

        $requestData = $request->getParams();
        $this->dataObjectHelper->populateWithArray($webjumpRegional, $requestData['general'], WebjumpRegionalInterface::class);

        $entityId = $this->webjumpRegionalRepository->save($webjumpRegional);

        return $entityId;
    }
}