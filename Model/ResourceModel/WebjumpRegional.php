<?php
/**
 * Copyright (C) 2020 Webjump
 *
 * This file included in Webjump/Regional is licensed under OSL 3.0
 *
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */
declare(strict_types=1);

namespace Webjump\Regional\Model\ResourceModel;


use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class WebjumpRegional extends AbstractDb
{
    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init('webjump_regional', 'entity_id');
    }
}