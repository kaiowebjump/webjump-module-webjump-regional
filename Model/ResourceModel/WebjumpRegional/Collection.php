<?php
/**
 * Copyright (C) 2019 Webjump
 *
 * This file included in Webjump/Regional is licensed under OSL 3.0
 *
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */
declare(strict_types=1);

namespace Webjump\Regional\Model\ResourceModel\WebjumpRegional;


use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Webjump\Regional\Model\WebjumpRegional;
use Webjump\Regional\Model\ResourceModel\WebjumpRegional as WebjumpRegionalResourceModel;

class Collection extends AbstractCollection
{
    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init(WebjumpRegional::class, WebjumpRegionalResourceModel::class);
    }
}