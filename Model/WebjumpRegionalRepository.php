<?php
/**
 * Copyright (C) 2019 Webjump
 *
 * This file included in Webjump/Regional is licensed under OSL 3.0
 *
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */
declare(strict_types=1);

namespace Webjump\Regional\Model;


use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Validation\ValidationException;
use Webjump\Regional\Api\Data\WebjumpRegionalInterface;
use Webjump\Regional\Api\Data\WebjumpRegionalSearchResultsInterface;
use Webjump\Regional\Api\Data\WebjumpRegionalSearchResultsInterfaceFactory;
use Webjump\Regional\Api\Data\WebjumpRegionalInterfaceFactory;
use Webjump\Regional\Api\WebjumpRegionalRepositoryInterface;
use Webjump\Regional\Model\ResourceModel\WebjumpRegional as WebjumpRegionalResourceModel;
use Webjump\Regional\Model\ResourceModel\WebjumpRegional\Collection;
use Webjump\Regional\Model\ResourceModel\WebjumpRegional\CollectionFactory;
use Webjump\Regional\Api\WebjumpRegionalValidatorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Psr\Log\LoggerInterface;

class WebjumpRegionalRepository implements WebjumpRegionalRepositoryInterface
{

    /**
     * @var WebjumpRegional
    */
    private $webjumpRegionalResource;

    /**
     * @var WebjumpRegionalValidatorInterface
    */
    private $webjumpRegionalValidator;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var WebjumpRegionalInterfaceFactory
    */
    private $webjumpRegionalInterfaceFactory;

    /**
     * @var CollectionFactory
    */
    private $webjumpRegionalCollection;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var WebjumpRegionalSearchResultsInterfaceFactory
    */
    private $webjumpRegionalSearchResultsInterfaceFactory;

    /**
     * @param WebjumpRegional $webjumpRegionalResource
     * @param WebjumpRegionalValidatorInterface $webjumpRegionalValidator
     * @param WebjumpRegionalInterfaceFactory $webjumpRegionalInterfaceFactory
     * @param CollectionFactory $webjumpRegionalCollection
     * @param CollectionProcessorInterface $collectionProcessor
     * @param LoggerInterface $logger
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param WebjumpRegionalSearchResultsInterfaceFactory $webjumpRegionalSearchResultsInterfaceFactory
    */
    public function __construct(
        WebjumpRegionalResourceModel $webjumpRegionalResource,
        WebjumpRegionalValidatorInterface $webjumpRegionalValidator,
        WebjumpRegionalInterfaceFactory $webjumpRegionalInterfaceFactory,
        CollectionFactory $webjumpRegionalCollection,
        CollectionProcessorInterface $collectionProcessor,
        LoggerInterface $logger,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        WebjumpRegionalSearchResultsInterfaceFactory $webjumpRegionalSearchResultsInterfaceFactory
    )
    {
        $this->webjumpRegionalResource = $webjumpRegionalResource;
        $this->webjumpRegionalValidator = $webjumpRegionalValidator;
        $this->webjumpRegionalInterfaceFactory = $webjumpRegionalInterfaceFactory;
        $this->webjumpRegionalCollection = $webjumpRegionalCollection;
        $this->collectionProcessor = $collectionProcessor;
        $this->logger = $logger;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->webjumpRegionalSearchResultsInterfaceFactory = $webjumpRegionalSearchResultsInterfaceFactory;
    }

    /**
     * @inheritdoc
     */
    public function save(WebjumpRegionalInterface $webjumpRegional): int
    {
        $isValid = $this->webjumpRegionalValidator->validate($webjumpRegional);

        if (!$isValid) {
            throw new ValidationException(__('Validation Failed.'));
        }

        try {
            $this->webjumpRegionalResource->save($webjumpRegional);
            return $webjumpRegional->getEntityId();
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            throw new CouldNotSaveException(__('Could not save Regional'), $e);
        }
    }

    /**
     * @inheritDoc
     */
    public function getById(int $webjumpRegionalId): WebjumpRegionalInterface
    {
        /** @var WebjumpRegionalInterface $webjumpRegional */
        $webjumpRegional = $this->webjumpRegionalInterfaceFactory->create();
        $this->webjumpRegionalResource->load($webjumpRegional, $webjumpRegionalId, $webjumpRegional::ENTITY_ID);

        if (null === $webjumpRegional->getEntityId()) {
            throw new NoSuchEntityException(__('Regional with id "%value" does not exist.', ['value' => $webjumpRegionalId]));
        }
        return $webjumpRegional;
    }

    /**
     * @inheritDoc
     */
    public function getList(SearchCriteriaInterface $searchCriteria = null): WebjumpRegionalSearchResultsInterface
    {
        /** @var Collection $collection */
        $collection = $this->webjumpRegionalCollection->create();

        if (null === $searchCriteria) {
            $searchCriteria = $this->searchCriteriaBuilder->create();
        } else {
            $this->collectionProcessor->process($searchCriteria, $collection);
        }

        /** @var WebjumpRegionalSearchResultsInterface $searchResult */
        $searchResult = $this->webjumpRegionalSearchResultsInterfaceFactory->create();
        $searchResult->setItems($collection->getItems());
        $searchResult->setTotalCount($collection->getSize());
        $searchResult->setSearchCriteria($searchCriteria);
        return $searchResult;
    }

    /**
     * @inheritDoc
     */
    public function deleteById(int $webjumpRegionalId): void
    {
        /** @var WebjumpRegionalInterface $webjumpRegional */
        $webjumpRegional = $this->webjumpRegionalInterfaceFactory->create();
        $this->webjumpRegionalResource->load($webjumpRegional, $webjumpRegionalId, WebjumpRegionalInterface::ENTITY_ID);

        if (null === $webjumpRegional->getEntityId()) {
            throw new NoSuchEntityException(
                __(
                    'There is no regional with "%fieldValue" for "%fieldName". Verify and try again.',
                    [
                        'fieldName' => WebjumpRegionalInterface::ENTITY_ID,
                        'fieldValue' => $webjumpRegionalId
                    ]
                )
            );
        }

        try {
            $this->webjumpRegionalResource->delete($webjumpRegional);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            throw new CouldNotDeleteException(__('Could not delete Regional'), $e);
        }
    }
}