<?php
/**
 * Copyright (C) 2019 Webjump
 *
 * This file included in Webjump/Regional is licensed under OSL 3.0
 *
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */
declare(strict_types=1);

namespace Webjump\Regional\Controller\Adminhtml\Regional;

use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Backend\Model\Session;


/**
 * NewAction Controller
 */
class NewAction extends Action implements HttpGetActionInterface
{
    /**
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Webjump_Regional::regional_view';
    /**
     * @var Session
     */
    private $session;

    /**
     * @inheritdoc
     */
    public function __construct(Action\Context $context, Session $session)
    {
        parent::__construct($context);
        $this->session = $session;
    }

    public function execute(): ResultInterface
    {
        $this->session->clearStorage();
        /** @var Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->setActiveMenu('Webjump_Regional::regional_view');
        $resultPage->getConfig()->getTitle()->prepend(__('New Regional'));

        return $resultPage;
    }
}
