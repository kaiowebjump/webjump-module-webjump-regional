<?php
/**
 * Copyright (C) 2019 Webjump
 *
 * This file included in Webjump/Regional is licensed under OSL 3.0
 *
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */
declare(strict_types=1);

namespace Webjump\Regional\Controller\Adminhtml\Regional;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Webjump\Regional\Api\Data\WebjumpRegionalInterface;
use Webjump\Regional\Api\WebjumpRegionalRepositoryInterface;
use Magento\Backend\Model\Session;

/**
 * Edit Controller
 */
class Edit extends Action implements HttpGetActionInterface
{
    /**
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Webjump_Regional::regional_edit';

    /**
     * @var WebjumpRegionalRepositoryInterface
     */
    private $webjumpRegionalRepository;

    /**
     * @var Session
    */
    private $session;

    /**
     * @param Context $context
     * @param WebjumpRegionalRepositoryInterface $webjumpRegionalRepository
     * @param Session $session
     */
    public function __construct(
        Context $context,
        WebjumpRegionalRepositoryInterface $webjumpRegionalRepository,
        Session $session
    ) {
        parent::__construct($context);
        $this->webjumpRegionalRepository = $webjumpRegionalRepository;
        $this->session = $session;
    }

    /**
     * @inheritdoc
     */
    public function execute(): ResultInterface
    {
        $regionalId = (int)$this->getRequest()->getParam(WebjumpRegionalInterface::ENTITY_ID);
        $this->session->setData(WebjumpRegionalInterface::PARAM_SESSION_REGIONAL_ENTITY_ID, $regionalId);
        try {
            $regional = $this->webjumpRegionalRepository->getById($regionalId);

            /** @var Page $result */
            $result = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
            $result->setActiveMenu('Webjump_Regional::regional_view')
                ->addBreadcrumb(__('Edit Regional'), __('Edit Regional'));
            $result->getConfig()
                ->getTitle()
                ->prepend(__('Edit Regional: %name', ['name' => $regional->getName()]));
        } catch (NoSuchEntityException $e) {
            /** @var Redirect $result */
            $result = $this->resultRedirectFactory->create();
            $this->messageManager->addErrorMessage(
                __('Regional with id "%value" does not exist.', ['value' => $regionalId])
            );
            $result->setPath('*/*');
        }

        return $result;
    }
}
