<?php
/**
 * Copyright (C) 2019 Webjump
 *
 * This file included in Webjump/Regional is licensed under OSL 3.0
 *
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */
declare(strict_types=1);

namespace Webjump\Regional\Controller\Adminhtml\Regional;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Validation\ValidationException;
use Webjump\Regional\Api\Data\WebjumpRegionalInterface;
use Webjump\Regional\Model\WebjumpRegionalProcessorSave;

/**
 * Save Controller
 */
class Save extends Action implements HttpPostActionInterface
{
    /**
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Webjump_Regional::regional_save';

    /**
     * @var WebjumpRegionalProcessorSave
     */
    private $webjumpRegionalProcessorSave;

    /**
     * @param Context $context
     * @param WebjumpRegionalProcessorSave $webjumpRegionalProcessorSave
     */
    public function __construct(
        Context $context,
        WebjumpRegionalProcessorSave $webjumpRegionalProcessorSave
    ) {
        parent::__construct($context);
        $this->webjumpRegionalProcessorSave = $webjumpRegionalProcessorSave;
    }

    /**
     * @inheritdoc
     */
    public function execute(): ResultInterface
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $request = $this->getRequest();
        $requestData = $request->getParams();
        if (!$request->isPost() || empty($requestData['general'])) {
            $this->messageManager->addErrorMessage(__('Wrong request.'));
            $this->processRedirectAfterFailureSave($resultRedirect);
            return $resultRedirect;
        }
        return $this->processSave($requestData, $request, $resultRedirect);
    }

    /**
     * @param array $requestData
     * @param RequestInterface $request
     * @param Redirect $resultRedirect
     * @return ResultInterface
     */
    private function processSave(
        array $requestData,
        RequestInterface $request,
        Redirect $resultRedirect
    ): ResultInterface {
        try {
            $webjumpRegionalId = isset($requestData['general'][WebjumpRegionalInterface::ENTITY_ID])
                ? (int)$requestData['general'][WebjumpRegionalInterface::ENTITY_ID]
                : null;

            $webjumpRegionalId = $this->webjumpRegionalProcessorSave->process($webjumpRegionalId, $request);

            $this->messageManager->addSuccessMessage(__('The Regional has been saved.'));
            $this->processRedirectAfterSuccessSave($resultRedirect, $webjumpRegionalId);
        } catch (NoSuchEntityException $e) {
            $this->messageManager->addErrorMessage(__('The Regional does not exist.'));
            $this->processRedirectAfterFailureSave($resultRedirect);
        } catch (ValidationException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->processRedirectAfterFailureSave($resultRedirect, $webjumpRegionalId);
        } catch (CouldNotSaveException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->processRedirectAfterFailureSave($resultRedirect, $webjumpRegionalId);
        } catch (InputException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->processRedirectAfterFailureSave($resultRedirect, $webjumpRegionalId);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('Could not save Regional.'));
            $this->processRedirectAfterFailureSave($resultRedirect, $webjumpRegionalId ?? null);
        }
        return $resultRedirect;
    }

    /**
     * @param Redirect $resultRedirect
     * @param int|null $webjumpRegionalId
     *
     * @return void
     */
    private function processRedirectAfterFailureSave(Redirect $resultRedirect, ?int $webjumpRegionalId = null): void
    {
        if (null === $webjumpRegionalId) {
            $resultRedirect->setPath('*/*/new');
        } else {
            $resultRedirect->setPath('*/*/edit', [
                WebjumpRegionalInterface::ENTITY_ID => $webjumpRegionalId,
                '_current' => true,
            ]);
        }
    }

    /**
     * @param Redirect $resultRedirect
     * @param int|null $webjumpRegionalId
     *
     * @return void
     */
    private function processRedirectAfterSuccessSave(Redirect $resultRedirect, ?int $webjumpRegionalId): void
    {
        if ($this->getRequest()->getParam('back')) {
            $resultRedirect->setPath('*/*/edit', [
                WebjumpRegionalInterface::ENTITY_ID => $webjumpRegionalId,
                '_current' => true,
            ]);
        } elseif ($this->getRequest()->getParam('redirect_to_new')) {
            $resultRedirect->setPath('*/*/new', [
                '_current' => true,
            ]);
        } else {
            $resultRedirect->setPath('*/*/');
        }
    }
}
