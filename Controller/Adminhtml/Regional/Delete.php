<?php
/**
 * Copyright (C) 2019 Webjump
 *
 * This file included in Webjump/Regional is licensed under OSL 3.0
 *
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */
declare(strict_types=1);

namespace Webjump\Regional\Controller\Adminhtml\Regional;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Webjump\Regional\Api\Data\WebjumpRegionalInterface;
use Webjump\Regional\Api\WebjumpRegionalRepositoryInterface;

/**
 * Delete Controller
 */
class Delete extends Action implements HttpPostActionInterface
{
    /**
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Webjump_Regional::regional_delete';

    /**
     * @var WebjumpRegionalRepositoryInterface
     */
    private $webjumpRegionalRepository;

    /**
     * @param Context $context
     * @param WebjumpRegionalRepositoryInterface $webjumpRegionalRepository
     */
    public function __construct(
        Context $context,
        WebjumpRegionalRepositoryInterface $webjumpRegionalRepository
    ) {
        parent::__construct($context);
        $this->webjumpRegionalRepository = $webjumpRegionalRepository;
    }

    /**
     * @inheritdoc
     */
    public function execute(): ResultInterface
    {
        $resultRedirect = $this->resultRedirectFactory->create();

        $webjumpRegionalId = $this->getRequest()->getPost(WebjumpRegionalInterface::ENTITY_ID);
        if ($webjumpRegionalId === null) {
            $this->messageManager->addErrorMessage(__('Wrong request.'));
            return $resultRedirect->setPath('*/*');
        }

        try {
            $webjumpRegionalId = (int)$webjumpRegionalId;
            $this->webjumpRegionalRepository->deleteById($webjumpRegionalId);
            $this->messageManager->addSuccessMessage(__('The Regional has been deleted.'));
            $resultRedirect->setPath('*/*');
        } catch (CouldNotDeleteException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $resultRedirect->setPath('*/*/edit', [
                WebjumpRegionalInterface::ENTITY_ID,
                '_current' => true,
            ]);
        }

        return $resultRedirect;
    }
}

